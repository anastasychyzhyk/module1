﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 test_class = new Module1();
            int[] test_mas=test_class.SwapItems(1, 2);
            Console.WriteLine(test_mas[0]+" "+ test_mas[1]);
            Console.WriteLine(test_class.GetMinimumValue(test_mas));
        }


        public int[] SwapItems(int a, int b)
        {
            int[] mas = new int[2];
            mas[0] = b;
            mas[1] = a;
            return mas;
        }

        public int GetMinimumValue(int[] input)
        {
            int count = input.Count();
            if (count == 1) return input[0];
            int min = input[0];
            for(int i=1; i<count; ++i)
            {
                if (input[i] < min)
                    min = input[i];
            }
            return min;
        }
    }
}
